/*
 Name:		SimLEDCube.cpp
 Created:	21.05.2019 17:50:13
 Author:	Filomaster
 Editor:	http://www.visualmicro.com
*/

#include "OurCube.h"
#include "Arduino.h"

//Constants to initialize defaults pins... consider changing them to #define directive
static const int default_DS = 8;
static const int default_STCP = 9;
static const int default_SHCP = 10;
/*
OurCube::OurCube()
{
	DS_PIN = default_DS;
	STCP_PIN = default_STCP;
	SHCP_PIN = default_SHCP;

	CubeSetup();
}*/
OurCube::OurCube(int DS_pin, int STCP_pin, int SHCP_pin)
{
	DS_PIN = DS_pin;
	STCP_PIN = STCP_pin;
	SHCP_PIN = SHCP_pin;

	CubeSetup();
}

void OurCube::Refresh() 
{
	digitalWrite(SHCP_PIN, LOW);
	digitalWrite(SHCP_PIN, HIGH);
}

void OurCube::SetPin(int Pin, int Value)
{
	REG[Pin] = Value;
}

void OurCube::Clear()
{
	for (int i = 0; i < CUBE_SIZE*CUBE_SIZE; i++)
	{
		REG[i] = LOW;
	}
	for (int i = CUBE_SIZE*CUBE_SIZE; i < CUBE_SIZE; i++)
	{
		REG[i] = HIGH;
	}
}

void OurCube::Write()
{
	digitalWrite(STCP_PIN, LOW);
	for (int i = PIN_COUNT - 1; i >= 0; i--) 
	{
		digitalWrite(DS_PIN, REG[i]);
	}
	digitalWrite(STCP_PIN, HIGH);
	Refresh();
}


void OurCube::CubeSetup()
{
	//REG_COUNT = ((CUBE_SIZE*(CUBE_SIZE + 1)) / 8 + 0.99);
	pinMode(DS_PIN, OUTPUT);
	pinMode(STCP_PIN, OUTPUT);
	pinMode(SHCP_PIN, OUTPUT);

	Clear();
	Refresh();
}
