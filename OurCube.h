#ifndef _OurCube_h
#define _OurCube_h

#include "Arduino.h"

#define CUBE_SIZE 3
#define REG_COUNT 2
#define PIN_COUNT REG_COUNT*8


class OurCube 
{
	public:
		//OurCube();
		OurCube(int DS_pin, int STCP_pin, int SHCP_pin);

		void Clear();
		void Write();
		void Refresh();
		void SetPin(int Pin, int Value);

	private:
		void CubeSetup();
		int DS_PIN;
		int STCP_PIN;
		int SHCP_PIN;
		//int REG_COUNT;
		//int PIN_COUNT = REG_COUNT * 8;
		//int CUBE_SIZE;
		int REG[PIN_COUNT];
};

#endif